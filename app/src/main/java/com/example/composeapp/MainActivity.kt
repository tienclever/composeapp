package com.example.composeapp

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.content.res.Configuration.UI_MODE_TYPE_WATCH
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composeapp.ui.theme.BaseTheme


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BaseTheme {
                Surface(
                    modifier = Modifier
                        .fillMaxHeight()
                        .fillMaxWidth()
                ) {
                    MyApp()
                }
            }
        }
    }
}

@Composable
fun MyApp() {
    var shouldShowBoarding by rememberSaveable { mutableStateOf(true) }
    if (shouldShowBoarding) {
        showTutorialScreen(onclick = { shouldShowBoarding = false })
    } else {
        FirstScreen()
    }
}

@Composable
fun rowItem(name: String) {
    var expanded by rememberSaveable { mutableStateOf(false) }

    val extraPadding by animateDpAsState(
        if (expanded) 100.dp else 0.dp,
        animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessLow
        )
    )

    Column(
        modifier = Modifier
            .padding(10.dp)
            .border(2.dp, Color.Yellow)
    ) {
        Surface(color = MaterialTheme.colors.secondary) {
            Row(
                modifier = Modifier
                    .padding(24.dp)
                    .animateContentSize(
                        animationSpec = spring(
                            dampingRatio = Spring.DampingRatioMediumBouncy,
                            stiffness = Spring.StiffnessLow
                        )
                    )
            ) {
                Card(
                    elevation = 4.dp,
                    shape = RoundedCornerShape(20.dp), border = BorderStroke(2.dp, Color.Black)
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_launcher_background),
                        contentDescription = "a"
                    )
                }

                Spacer(modifier = Modifier.padding(10.dp))

                Column(
                    modifier = Modifier
                        .weight(1f)
                        .padding(bottom = extraPadding.coerceAtLeast(0.dp)),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(text = "Hello,", style = MaterialTheme.typography.body2)
                    Text(text = name, style = MaterialTheme.typography.body2)
                }
                Button(
                    onClick = { expanded = !expanded }
                ) {
                    Text(if (expanded) stringResource(R.string.hind) else stringResource(R.string.show))
                }
            }
        }
    }
}

@Composable
fun showTutorialScreen(onclick: () -> Unit) {
    Surface {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                "Welcome to the Basics Codelab!", style = MaterialTheme.typography.overline.copy(
                    fontWeight = FontWeight.ExtraBold,
                    color = MaterialTheme.colors.onSecondary
                )
            )
            Button(
                modifier = Modifier.padding(vertical = 24.dp),
                onClick = { onclick() }
            ) {
                Text("Continue", color = Color(0xFFFF5722))
            }
        }
    }
}

@Preview(
    showBackground = true,
    widthDp = 320,
    heightDp = 640,
    uiMode = UI_MODE_NIGHT_YES,
    name = "dark mode"
)
@Preview(
    showBackground = true,
    widthDp = 320,
    heightDp = 640,
    uiMode = UI_MODE_TYPE_WATCH,
    name = "light mode"
)
@Composable
fun DefaultPreview() {
    BaseTheme {
        MyApp()
    }
}

@Preview(heightDp = 640, widthDp = 500)
@Composable
private fun FirstScreen(names: List<String> = List(10) { "$it" }) {
    Column {
        LazyRow(modifier = Modifier.padding(10.dp)) {
            items(names) {
                rowItem(name = it)
            }
        }
        LazyColumn(modifier = Modifier.padding(10.dp)) {
            items(names) {
                rowItem(name = it)
            }
        }
    }
}